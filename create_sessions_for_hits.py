#
# Author: Dingquan Yu 
# Create chimera sessions for L fragment and its interactor vs L structures 
# #
import pandas as pd
from chimera import runCommand as rc




def fetch_l_pds(L_pdbs):
    """ 
    Fetch all structures of L from PDB
    update:
    align the rest of pdbs to the first one 7ojn (#0)
    """
    for pdb in L_pdbs:
        rc("open "+pdb)

    rc("mm #0 #1-{}".format(len(L_pdbs)))

def align_model_to_pdb(corr_chains):
    """align predicted model with pdb structures"""
    for i in range(len(corr_chains)):
        chain_id = corr_chains[i]
        model_num = i + len(corr_chains) +1
        cmd = "mmaker #{}:.{}, #{}".format(i,chain_id,model_num)
        print("command is {}".format(cmd))
        rc(cmd)

def create_sessions(host_uniprot,l_fragment,L_pdbs,results_dir,corr_chains):
    """open predicted L fragments and host proteins model"""
    fetch_l_pds(L_pdbs)
    subdir = "{}/{}/{}_yes".format(results_dir,host_uniprot,l_fragment)
    for i in range(len(L_pdbs)):
        rc("open {}/ranked_0.pdb".format(subdir))
    
    align_model_to_pdb(corr_chains)
    print("finished for {}".format(host_uniprot))
    output_dir='/home/geoffrey/create_chemira_sessions/L_fragment_vs_31_host_full_length_good_PAE_L_aligned'
    rc("save {}/{}-{}.py".format(output_dir,host_uniprot,l_fragment))
 



def main():
    ## First open all LASV L protein full length structures
    L_pdbs = ['7OJN','7ELA','7OJK','7OEB','7OEA','7OJL','7CKL','7OJJ','7OE3','7OCH','7OE7','6KLC']
    corr_chains = ['A','A','L','L','L','L','A','L','L','L','A']

    ## Then open the models predicted 
    full_length_hosts_good_path = '/media/geoffrey/bigdata/g/kosinski/geoffrey/af2_lasv_interactome/af2_lasv_L_apms/scripts/evaluation/predicted_interactors_from_31_shortlists.csv'
    df = pd.read_csv(full_length_hosts_good_path)
    print("df shape is {}".format(df.shape))
    results_dir = '/media/geoffrey/bigdata/scratch/gyu/af2_lasv_L_apms_result'
    for i in range(df.shape[0]):
        rc("close all")
        host = df.iloc[i,1]
        fragment = df.iloc[i,2]
        create_sessions(host,fragment,L_pdbs,results_dir,corr_chains)
if __name__ == "__main__":
    main()