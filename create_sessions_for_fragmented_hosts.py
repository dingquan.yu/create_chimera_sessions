#
# Author: Dingquan Yu 
# Create chimera sessions for L fragment and fragmented interactor vs L structures 
# 
# #
import pandas as pd
from chimera import runCommand as rc

import json

def get_combos():
    """Get fragmented hosts and their corresponding L fragments"""
    cells = json.load(open("/home/geoffrey/programme_notebook/L_protein_VS_31_hosts_fragments_good.json"))
    cells = cells['cells']
    wanted_cells = [c for c in cells if c['cell_type']=='code']
    wanted_combo = []
    for w in wanted_cells:
        host_fragment = w['source'][0].split("/")[-3]
        L_fragment = w['source'][0].split("/")[-2].split("_yes")[0].split("L_")[1]
        wanted_combo.append("{}:{}".format(host_fragment,L_fragment))
    return wanted_combo

def fetch_l_pds(L_pdbs):
    """Fetch all structures of L from PDB"""
    for pdb in L_pdbs:
        rc("open "+pdb)
    rc("mm #0 #1-{}".format(len(L_pdbs)))
    
def align_model_to_pdb(corr_chains):
    """align predicted model with pdb structures"""
    for i in range(len(corr_chains)):
        chain_id = corr_chains[i]
        model_num = i + len(corr_chains) + 1
        cmd = "mmaker #{}:.{}, #{}".format(i,chain_id,model_num)
        rc(cmd)

def create_sessions(wanted_combo,L_pdbs,results_dir,corr_chains):
    """open predicted L fragments and host proteins model"""
    for combo in wanted_combo:
        rc("close all") # firstly close all the opened models 
        fetch_l_pds(L_pdbs)
        host_uniprot = combo.split(":")[0]
        l_fragment = combo.split(":")[1]
        subdir = "{}/{}/L_{}".format(results_dir,host_uniprot,l_fragment)
        for i in range(len(L_pdbs)):
            rc("open {}/ranked_0.pdb".format(subdir))
        
        align_model_to_pdb(corr_chains)
        print("finished for {}".format(host_uniprot))
        output_dir='/home/geoffrey/create_chemira_sessions/L_fragment_vs_31_fragmented_host_good_PAE/'
        rc("save {}/{}-L_{}.py".format(output_dir,host_uniprot,l_fragment))



def main():
    ## First open all LASV L protein full length structures
    L_pdbs = ['7OJN','7ELA','7OJK','7OEB','7OEA','7OJL','7CKL','7OJJ','7OE3','7OCH','7OE7','6KLC']
    corr_chains = ['A','A','L','L','L','L','A','L','L','L','A']

    ## Then open the models predicted 
    wanted_combos = get_combos()
    results_dir = '/media/geoffrey/bigdata/scratch/gyu/af2_lasv_L_31_shortlists'
    create_sessions(wanted_combos,L_pdbs,results_dir,corr_chains)

if __name__ == "__main__":
    main()